import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, NgForm } from '@angular/forms';

import { CreditCardFormComponent } from './credit-card-form.component';
import { Component, ViewChild } from '@angular/core';
import { WebAppDTO } from 'src/app/shared/dto/web-app-dto';
import { CreditCardNumberFormatterDirective } from 'src/app/shared/directives/credit-card/credit-card-number-formatter.directive';
import { CreditCardCVVFormatterDirective } from 'src/app/shared/directives/credit-card/credit-card-cvv-formatter.directive';

describe('CreditCardFormComponent', () => {
  let component: CreditCardFormComponent;
  let fixture: ComponentFixture<CreditCardFormComponent>;
  let testForm: NgForm;

  beforeEach(async(() => {
    testForm = new NgForm(null, null);

    TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      declarations: [
        CreditCardFormComponent,
        CreditCardNumberFormatterDirective,
        CreditCardCVVFormatterDirective
      ],
      providers: [
        { provide: NgForm, useValue: testForm }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditCardFormComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('credit card info validation succeed', () => {
    component.creditCard = { ccType: 'Mastercard', ccNumber: '5424000000000015', expirationYear: '2028', expirationMonth: '10', cvv: '333' };
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      testForm.onSubmit(null);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(component.form.valid).toBeTruthy();
        fail();
      }, error => {
        fail();
      });
    });
  });

  it('credit card number wrong', () => {
    component.creditCard = { ccType: 'Visa', ccNumber: '0000000000000000', expirationYear: '2029', expirationMonth: '01', cvv: '123' };
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      testForm.onSubmit(null);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(component.form.valid).toBeFalsy();
        expect(component.form.invalid).toBeTrue();
        let txtCcNumber = component.form.controls['txtCcNumber'];
        expect(txtCcNumber.valid).toBeFalsy();
        fail();
      }, error => {
        fail();
      });
    });
  });

  it('credit card expiration date wrong', () => {
    component.creditCard = { ccType: 'American Express', ccNumber: '370000000000002', expirationYear: '2029', expirationMonth: '', cvv: '1234' };
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      testForm.onSubmit(null);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(component.form.valid).toBeFalsy();
        expect(component.form.invalid).toBeTrue();
        let expirationMonth = component.form.controls['expirationMonth'];
        expect(expirationMonth.valid).toBeFalsy();
        fail();
      }, error => {
        fail();
      });
    });
  });

  it('credit card cvv wrong', () => {
    component.creditCard = { ccType: 'American Express', ccNumber: '370000000000002', expirationYear: '2029', expirationMonth: '12', cvv: 'abc' };
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      testForm.onSubmit(null);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(component.form.valid).toBeFalsy();
        expect(component.form.invalid).toBeTrue();
        let cvv = component.form.controls['cvv'];
        expect(cvv.valid).toBeFalsy();
        fail();
      }, error => {
        fail();
      });
    });
  });
});

@Component(
  {
    selector: 'test-cmp',
    template: '<form #form="ngForm" (ngSubmit)="submit()"><app-credit-card-form [creditCard]="creditCard" [parentForm]="testForm"></app-credit-card-form><input type="submit" id="btnSubmit" value="Save" /></form>'
  })
class TestComponent {
  @ViewChild('form') public testForm: NgForm;
  public creditCard: WebAppDTO.ICreditCard;
  public submit(): void { }
}