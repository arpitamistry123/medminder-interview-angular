import { Component, OnInit, Input } from '@angular/core';
import { NgForm, NgModelGroup } from '@angular/forms';
import { WebAppDTO } from 'src/app/shared/dto/web-app-dto';
@Component({
  selector: 'app-credit-card-form',
  templateUrl: './credit-card-form.component.html',
  styleUrls: ['./credit-card-form.component.sass']
})
export class CreditCardFormComponent implements OnInit {
  @Input() public creditCard: WebAppDTO.ICreditCard;
  public years: number[] = [2021, 2022, 2023, 2024];
  constructor(public form: NgForm) {
  }
  ngOnInit(): void {

  }


}
